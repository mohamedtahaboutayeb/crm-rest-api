"""crm_rest_api URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
# IMPORTS

# DJANGO
from django.conf.urls import url
from django.urls import path
from django.conf.urls import include
from django.contrib import admin
from rest_framework.routers import DefaultRouter

# PROJECT
from account.views import UserView
from activity.views import ActivityView
from company.views import CompanyView
# *****************************************************************************
# URLS
# https://docs.djangoproject.com/en/1.11/topics/http/urls/
# *****************************************************************************

# API endpoints
router = DefaultRouter()

router.register(r'users', UserView, base_name='user')
router.register(r'activities',ActivityView,base_name='activity')
router.register(r'companies',CompanyView,base_name='company')
# Url patterns
urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include((router.urls, 'api')))

]
