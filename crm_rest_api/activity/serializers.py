# IMPORTS


# REST_FRAMEWORK
from rest_framework import serializers

# PROJECT
from .models import Activity


# SERIALIZERS
# http://www.django-rest-framework.org/api-guide/serializers/


class ActivitySerialiser(serializers.ModelSerializer):
    """A serializer for Activity objects."""

    class Meta:
        model = Activity
        fields = (
            'activity_type',
            'date_activity',
            'user'

        )
        read_only_fields = (
            'date_created',
            'date_deleted',
            'date_updated',
        )
