# Imports

# DJANGO
from django.db import models

# PROJECT
from account.models import User

# Model


class Activity(models.Model):
    """Activity Model"""

    # Attributes
    TYPE_CHOICES = (
        ('AT', 'Appel Telephonique'),
        ('RDV', 'Reunion')
    )
    activity_type = models.CharField(
        max_length=3,
        choices=TYPE_CHOICES
    )
    comment = models.CharField(max_length=1024)
    date_activity = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Relationships
    user = models.ForeignKey(User, related_name='activities',on_delete=models.CASCADE)

    # Methods
    def __str__(self):
        """Return the Activity date"""

        return self.activity_type
