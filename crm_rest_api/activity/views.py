# *****************************************************************************
# IMPORTS
# *****************************************************************************


# DJANGO
from rest_framework.viewsets import ModelViewSet

# PROJECT
from .models import Activity
from .serializers import ActivitySerialiser


# *****************************************************************************
# VIEWS
# http://www.django-rest-framework.org/api-guide/views/
# *****************************************************************************

class ActivityView(ModelViewSet):
    """View for the Activity Model."""

    serializer_class = ActivitySerialiser
    queryset = Activity.objects.all()
