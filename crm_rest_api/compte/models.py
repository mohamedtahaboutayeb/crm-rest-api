# Imports

# DJANGO
from django.db import models


# PROJECT
from company.models import Company



class Compte(models.Model):
    """Compte Model"""


    #Attributes
    CP = 'client potentiel'
    CL = 'client'
    FR = 'fournisseur'
    DIST = 'distributeur'
    ASS  = 'Associé'
    CLIENT_CHOICES = (
            (CP,'client potentiel'),
            (CL,'client'),
            (FR,'fournisseur'),
            (DIST,'distributeur'),
            (ASS,'Associé')
    )
    type = models.CharField(
        max_length=255,
        choices=CLIENT_CHOICES
    )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    Fax = models.CharField(max_length=12)
    phone_number = models.CharField(max_length=12)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    code_postal = models.CharField(max_length=15)
    Adress = models.CharField(max_length=255)
    country = models.CharField(max_length=255)
    area = models.CharField(max_length=255)

    #Relationships
    company = models.ForeignKey(Company, related_name='comptes',on_delete=models.CASCADE)
    # Methods
    def __str__(self):
        """return the compte email"""
        return self.email
