# Generated by Django 2.0 on 2018-08-31 13:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('company', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Compte',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('type', models.CharField(choices=[('client potentiel', 'client potentiel'), ('client', 'client'), ('fournisseur', 'fournisseur'), ('distributeur', 'distributeur'), ('Associé', 'Associé')], max_length=255)),
                ('email', models.EmailField(max_length=255, unique=True, verbose_name='email address')),
                ('Fax', models.CharField(max_length=12)),
                ('phone_number', models.CharField(max_length=12)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('code_postal', models.CharField(max_length=15)),
                ('Adress', models.CharField(max_length=255)),
                ('country', models.CharField(max_length=255)),
                ('area', models.CharField(max_length=255)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comptes', to='company.Company')),
            ],
        ),
    ]
