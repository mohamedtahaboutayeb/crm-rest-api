# IMPORTS


# REST_FRAMEWORK
from rest_framework import serializers

# PROJECT
from .models import Company


# SERIALIZERS
# http://www.django-rest-framework.org/api-guide/serializers/


class CompanySerialiser(serializers.ModelSerializer):
    """A serializer for Company objects."""

    class Meta:
        model = Company
        fields = (
            'name',
            'size_team'


        )
        read_only_fields = (
            'date_created',
            'date_deleted',
            'date_updated',
        )
