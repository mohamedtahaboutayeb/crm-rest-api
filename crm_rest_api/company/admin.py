# Imports
from django.contrib import admin
from .models import Company

# Register Company model.

admin.site.register(Company)
