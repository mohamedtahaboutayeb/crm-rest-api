# Imports
from django.db import models

# Model


class Company(models.Model):
    """Company Model"""

    name = models.CharField(max_length=30)
    size_team = models.IntegerField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return the company name"""

        return self.name
