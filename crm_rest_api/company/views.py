# *****************************************************************************
# IMPORTS
# *****************************************************************************


# DJANGO
from rest_framework.viewsets import ModelViewSet

# PROJECT
from .models import Company
from .serializers import CompanySerialiser


# *****************************************************************************
# VIEWS
# http://www.django-rest-framework.org/api-guide/views/
# *****************************************************************************

class CompanyView(ModelViewSet):
    """View for the Company Model."""

    serializer_class = CompanySerialiser
    queryset = Company.objects.all()
