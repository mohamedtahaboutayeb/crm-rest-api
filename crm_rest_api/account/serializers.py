# IMPORTS


# REST_FRAMEWORK
from rest_framework import serializers

# PROJECT
from .models import User


# SERIALIZERS
# http://www.django-rest-framework.org/api-guide/serializers/


class UserSerialiser(serializers.ModelSerializer):
    """A serializer for User objects."""

    class Meta:
        model = User
        fields = (
            'email',
            'last_name',
            'first_name',
            'phone_number',
            'company'
        )
        read_only_fields = (
            'date_created',
            'date_deleted',
            'date_updated',
        )
