# *****************************************************************************
# IMPORTS
# *****************************************************************************


# DJANGO
from rest_framework.viewsets import ModelViewSet

# PROJECT
from .models import User
from .serializers import UserSerialiser


# *****************************************************************************
# VIEWS
# http://www.django-rest-framework.org/api-guide/views/
# *****************************************************************************


class UserView(ModelViewSet):
    """View for the User Model."""

    serializer_class = UserSerialiser
    queryset = User.objects.all()
