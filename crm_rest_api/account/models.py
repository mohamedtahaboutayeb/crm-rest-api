# Imports

# DJANGO
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)

# PROJECT
from company.models import Company


# Model


class UserManager(BaseUserManager):
    def create_user(self, email, last_name, first_name, phone_number, password, company):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )
        user.last_name = last_name
        user.first_name = first_name
        user.phone_number = phone_number
        user.company = company
        user.set_password(password)
        user.save(using=self._db)  # protected: _ private: __ public:
        return user

    def create_staffuser(self, email, last_name, first_name, phone_number, password, company):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
            last_name=last_name,
            first_name=first_name,
            phone_number=phone_number,
            company=company
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, email, last_name, first_name, phone_number, password ):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            email,
            password=password,
            last_name=last_name,
            first_name=first_name,
            phone_number=phone_number

        )
        user.staff = True
        user.admin = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):

    # Attributes
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    last_name = models.CharField(max_length=50)
    first_name = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=12)  # Must be verified
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False)
    admin = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Relationships
    company = models.ForeignKey(Company, related_name='accounts',on_delete=models.CASCADE)

    # Management
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['last_name', 'first_name', 'phone_number']

    objects = UserManager()

    # Methods
    def get_full_name(self):
        """The user is identified by his lastname & firstname"""
        return self.last_name + ' ' + self.first_name

    def get_short_name(self):
        """The user is identified by his lastname"""
        return self.last_name

    def __str__(self):
        """The user is identified by his email"""
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        """Is the user a member of staff?"""
        return self.staff

    @property
    def is_admin(self):
        """Is the user a admin member?"""
        return self.admin

    @property
    def is_active(self):
        """Is the user active?"""
        return self.active
