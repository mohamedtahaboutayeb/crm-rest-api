# Imports

# DJANGO
from django.db import models

# PROJECT
from account.models import User

# Model


class Opportunity(models.Model):
    """Opportunity Model"""

    # Attributes
    name = models.CharField(max_length=255)
    comment = models.CharField(max_length=1024)
    value = models.FloatField()
    probability = models.IntegerField()
    date_sell_estimited = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Relationships
    user = models.ForeignKey(User, related_name='opportunities',on_delete=models.CASCADE)

    # Methods
    def __str__(self):
        """Return the Opportunity name"""

        return self.name
