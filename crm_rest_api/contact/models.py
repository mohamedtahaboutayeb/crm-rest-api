# Imports

# DJANGO
from django.db import models

# PROJECT
from company.models import Company




class Contact(models.Model):
    """Contact Model"""

    # Attributes
    HOMME = 'H'
    FEMME = 'F'
    CIVILITE_CHOICES = (
        (HOMME, 'homme'),
        (FEMME, 'femme')
    )
    civilite = models.CharField(
        max_length=2,
        choices=CIVILITE_CHOICES
    )
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    phone_number = models.CharField(max_length=12)  # Must be verified
    poste = models.CharField(max_length=255)
    skype = models.CharField(max_length=255)
    linkedin = models.CharField(max_length=255)
    commentaires = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    # Relationships
    company = models.ForeignKey(Company, related_name='contacts',on_delete=models.CASCADE)

    # Methods
    def __str__(self):
        """return the contact email"""
        return self.email
