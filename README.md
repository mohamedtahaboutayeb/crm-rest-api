# CRM REST API

REST API built with Django Rest Framework and Postgres database in order to serve Web and Mobile applications.

## Prerequisites

In order to run the project in a local environment all you need is Docker and docker-compose tool installed.
You may also want to have pipenv tool installed to run the project in a virtual environment

## Building the project:

Go to the project directory where `docker-compose.yaml` and `Dockerfile` are located and run the command:

```bash
$ sudo docker-compose build
```

# Running the server locally

You can run the project using docker-compose tool:

```bash
# You must build the project first
$ sudo docker-compose up
# Else, you can run the project and build it at the same time
$ sudo docker-compose up --build
```
